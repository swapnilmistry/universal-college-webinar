const Ajv = require("ajv");
const router = require("express").Router();
const { users, userSchema } = require("./contants");

const ajv = new Ajv();
const validateUser = ajv.compile(userSchema);

router.get("/", async (req, res) => {
  return res.status(200).send({
    message: "Welcome to first node API.",
  });
});

router.get("/users", async (req, res) => {
  res.status(200).send(users);
});

router.get("/user/:id", async (req, res) => {
    const { id } = req.params;
    const idInit = parseInt(id, 10);
    const userIndex = users.findIndex(x => x.id === idInit);
    if (userIndex !== -1) {
        return res.status(200).send(users[userIndex]);
    }
    return res.status(404).send({
        message: `User with id ${id} dose not exists.`
    })
})

router.post("/user", async (req, res) => {
    if (validateUser(req.body)) {
        const userData = {
            id: users.length + 1,
            ...req.body
        }
        users.push(userData);
        return res.status(201).send({
            message: "User created successfully!",
            id: userData.id,
            data: userData
        })
    }
    return res.status(400).send({
        message: "Bad request. Missing some schema parameters."
    })
})

router.put("/user/:id", async (req, res) => {
    /**
     * Validate user exists or not
     * Update record { ...existingUserData, ...newDataInPost  }
     * Update value at that index
     * Send the response
     */
})

router.delete("/user/:id", async (req, res) => {
    const { id } = req.params;
    const idInt = parseInt(id, 10);
    const userIndex = users.findIndex(x => x.id === idInt);
    if (userIndex !== -1) {
        const deletedUser = users[userIndex];
        users.splice(userIndex, 1);
        return res.status(200).send({
            message: `User with id ${id} deleted successfully!`,
            data: deletedUser
        })
    }
    return res.status(404).send({
        message: `Cannot delete user as i dose not exits with id ${id}.`
    })
})

module.exports = router;
