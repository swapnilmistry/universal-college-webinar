const express = require("express");
const bodyParser = require("body-parser");
const routes = require("./routes");
const { port } = require("./contants");

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(routes);

app.listen(port, () => {
  console.log(`Listening on ${port}`);
});
